﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class earthRotation : MonoBehaviour
{
    public float timeScale = 8;  // Multiplier for time scale
    private float earthRotationPerFrame;
    private float degreesToRotatePerFrame;

    // Start is called before the first frame update
    void Start()
    {
        earthRotationPerFrame = 0.004f;  // 360 degrees in 24hrs, 60 mins/hour, 60sec/min, 60frame/sec
        degreesToRotatePerFrame = earthRotationPerFrame * timeScale;
        Debug.Log(earthRotationPerFrame);
    }

    // Update is called once per frame
    void Update()
    {
        // Apply rotation per frame
        transform.Rotate(0, degreesToRotatePerFrame, 0, Space.Self);


        //Vector3 to = new Vector3(amountToRotatePerFrame, 0, 0);
        //transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, to, Time.deltaTime);


    }
}
