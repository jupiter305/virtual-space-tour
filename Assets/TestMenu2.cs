﻿/* Example level loader */
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TestMenu2 : MonoBehaviour
{

    void OnGUI()
    {
        // Make a background box
        GUI.Box(new Rect(Screen.width / 2, Screen.height / 2, 200, 90), "Loader Menu");

        // Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
        if (GUI.Button(new Rect(Screen.width / 2 + 10, Screen.height / 2 + 40, 180, 20), "Start Scenario"))
        {
            SceneManager.LoadScene("Space", LoadSceneMode.Single);
        }
    }
}