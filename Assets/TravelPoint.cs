﻿// Stores data on what to do with this travel point

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelPoint : MonoBehaviour
{
    public GameObject destinationObject;
    public GameObject toFaceAtEnd;
}
