﻿// Scenario. Compiles the indivual ScenarioPoints in the object into 1 queue.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario : MonoBehaviour
{
    // Gets all ScenarioPoint components in this Object and adds them to a Queue
    public Queue<ScenarioPoint> GetScenarioPoints()
    {
        Queue<ScenarioPoint> scenarioPoints = new Queue<ScenarioPoint>();
        ScenarioPoint[] components = GetComponents<ScenarioPoint>();
        for (int i = 0; i < components.Length; i++)
        {
            scenarioPoints.Enqueue(components[i]);
        }
        return scenarioPoints;
    }
}



