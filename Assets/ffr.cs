﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;
public class OculusSettings : MonoBehaviour {
    // Use this for initialization
    void Start () {
        //OVRManager.fixedFoveatedRenderingLevel = OVRManager.FixedFoveatedRenderingLevel.Medium;
        OVRManager.display.displayFrequency = 72.0f;
    }
}
