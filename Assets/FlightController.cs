﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlightController : MonoBehaviour
{
    public GameObject shipPosition;
    private Vector3 rotateTargetVector;
    private bool rotate, move = false;
    private TravelPoint tp;
    //float step;
    //float rotateSpeed = 1f;
    float speed = 5f;

    private void Update()
    {

        // Rotate towards target
        if (rotate)
        {
            if (Vector3.Distance(shipPosition.transform.eulerAngles, rotateTargetVector) > 0.01f)
            {
                shipPosition.transform.eulerAngles = Vector3.Lerp(shipPosition.transform.rotation.eulerAngles, rotateTargetVector, Time.deltaTime);
            }
            else
            {
                shipPosition.transform.eulerAngles = rotateTargetVector;
                rotate = false;
            }
        }


        
        // Movement
        if (move)
        {
            // The step size is equal to speed times frame time.
            float step = speed * Time.deltaTime;
            Vector3 targetDir = tp.destinationObject.transform.position - shipPosition.transform.position;    
            // Move our position a step closer to the target.
            step = speed * Time.deltaTime; // calculate distance to move
            shipPosition.transform.position = Vector3.MoveTowards(shipPosition.transform.position, tp.destinationObject.transform.position, step);


            /* REMOVING ROTATION BECAUSE IT CAUSES SICKNESS
            Vector3 targetDir = tp.destinationObject.transform.position - shipPosition.transform.position;

            Vector3 newDir = Vector3.RotateTowards(shipPosition.transform.forward, targetDir, step, 0.0f);

            // Move our position a step closer to the target.
            shipPosition.transform.rotation = Quaternion.LookRotation(newDir);

            if (Vector3.Distance(shipPosition.transform.position, tp.destinationObject.transform.position) > 0.005f)
            {
                // Rotate towards target
                Vector3 targetDir = tp.destinationObject.transform.position - shipPosition.transform.position;
                step = rotateSpeed * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(shipPosition.transform.forward, targetDir, step, 0.0f);
                shipPosition.transform.rotation = Quaternion.LookRotation(newDir);

                // Move our position a step closer to the target.
                step = speed * Time.deltaTime; // calculate distance to move
                shipPosition.transform.position = Vector3.MoveTowards(shipPosition.transform.position, tp.destinationObject.transform.position, step);
            }
            else
            {
                // Rotate toward end object if we've arrived at the destination, if it exists
                if (tp.toFaceAtEnd)
                {
                    Vector3 targetDir = tp.toFaceAtEnd.transform.position - shipPosition.transform.position;
                    step = rotateSpeed * Time.deltaTime;
                    Vector3 newDir = Vector3.RotateTowards(shipPosition.transform.forward, targetDir, step, 0.0f);
                    shipPosition.transform.rotation = Quaternion.LookRotation(newDir);

                    if (newDir[0] < 0.1f && newDir[2] < 0.1f)
                    {
                        // When we reach the end, set Move to false -- we're done
                        Debug.Log(newDir);
                        move = false;

                        // Level out our rotation 
                        LevelRotation();
                    }
                }
            }*/
        }
    }

    // Rotate ship on y axis
    public void RotateAmt(float degrees)
    {
        rotate = true;
        rotateTargetVector = shipPosition.transform.rotation.eulerAngles + new Vector3(0, degrees, 0);
    }

    // Make ship be level again on non-y axis
    public void LevelRotation()
    {
        rotate = true;
        rotateTargetVector = new Vector3(0, 0, 0);
        Debug.Log(shipPosition.transform.rotation.eulerAngles);
    }


    // Fly to a given travel point, using its data
    public void FlyToTravelPoint(TravelPoint travelPoint)
    {
        rotate = false;
        move = true;
        tp = travelPoint;
        GetComponent<AudioSource>().Play();
    }

    // Instantly sets ship position
    public void SetShipPos(Vector3 pos)
    {
        shipPosition.transform.position = pos;
    }
    // Instantly sets ship rotation
    public void SetShipRot(Vector3 rot)
    {
        shipPosition.transform.rotation = Quaternion.Euler(rot.x, rot.y, rot.z);
    }
}