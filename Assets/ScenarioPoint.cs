﻿// A class read by the Scenario class. Determines the timing of when events and subtitles occur.
// Based on Scenarios from Design portfolio excel sheets. Each one of these is equivalent to 1 row in the sheet.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class ScenarioPoint : MonoBehaviour
{
    public float timeDelay = 0f;  // Amount of time after the last event to do this event
    public string subtitlePrefix = "Ship AI: ";  // To be concatenated to subtitleText (is here for convenience)
    public string subtitleText = "";
    //public AudioClip voiceover; 
    public Vector3 setShipPosition;
    public Vector3 setShipRotation;
    public float setTimescale;
    public float setMoonPosition;

}
