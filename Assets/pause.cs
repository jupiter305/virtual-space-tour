﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pause : MonoBehaviour
{
    public MoonOrbit moon;
    private bool paused = false;
    

    public void PausePressed() {
        Time.timeScale = 0f;
        paused = true;
    }
    public void ResumePressed() {
        Time.timeScale = 1.0f;
        paused = true;
    }
}
