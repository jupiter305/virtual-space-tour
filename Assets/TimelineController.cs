﻿// Timeline controller -- the heart of the operation!
// It parses pre-determined events and makes them appear at the desired times.
// In this version there is no going back in time as we are using a queue.
// In the future, another structure should be used to allow backwards time travel.
// (things happening at a set time rather than x seconds after the last thing happened.)
// (this is the reason it was initially called Timeline)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimelineController : MonoBehaviour
{
    public SubtitleController subtitleController;
    public FlightController flightController;
    public PlanetController planetController;
    public SimController simController;
    public SoundController soundController;

    private Queue<ScenarioPoint> scenarioPoints;

    // Starts the timeline by giving it a Queue to start off from.
    public void StartTimeline(Queue<ScenarioPoint> scePts)
    {
        scenarioPoints = scePts;
        DoNextInQueue();
    }

    // Take the next thing in queue and do what it tells us to do. 
    // Wait until it's done, then go next in line... so on.
    void DoNextInQueue()
    {
        if (scenarioPoints.Count > 0)
        {
            // Do functions after the specified amount of delay.
            ScenarioPoint currentPoint = scenarioPoints.Dequeue();
            StartCoroutine("WaitThenDo", currentPoint);
        }
        else
        {
            // End the scenario if there are no more ScenarioPoints left.
            EndScenario();
        }
    }

    IEnumerator WaitThenDo(ScenarioPoint currentPoint)
    {
        // Get data.
        string subtitleString = currentPoint.subtitlePrefix + currentPoint.subtitleText;
        float waitTime = currentPoint.timeDelay;
        float newTimescale = currentPoint.setTimescale;
        float newMoonPos = currentPoint.setMoonPosition;
        Vector3 newShipPos = currentPoint.setShipPosition;
        Vector3 newShipRot = currentPoint.setShipRotation;

        // Wait for delay amount.
        yield return new WaitForSeconds(waitTime);

        // Run point functions.
        if (subtitleString != "")
        {
            subtitleController.ShowText(subtitleString);
            Debug.Log(subtitleString);
        }
        if (newTimescale != 0)
        {
            simController.SetTimescale(newTimescale);
        }
        if (newMoonPos != 0)
        {
            planetController.SetMoonTime(newMoonPos);
        }
        if (newShipPos != new Vector3(0,0,0))
        {
            flightController.SetShipPos(newShipPos);
        }
        if (newShipRot != new Vector3(0, 0, 0))
        {
            flightController.SetShipRot(newShipRot);
        }

        // Get the next ScenarioPoint in line.
        DoNextInQueue();
        yield return null;
    }

    void EndScenario()
    {
        Debug.Log("End of scenario. Make popup asking if user wants to restart or return to menu here.");
    }
}
