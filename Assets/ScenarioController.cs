﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioController : MonoBehaviour
{
    public TimelineController timelineController;
    //public Scenario scenarioToPlay;  // The scenario you want to play.
    public Scenario[] scenarios;

    private void Start()
    {
        // Choose scenario based on menu selection.
        // Hacky. Change ASAP
        ScenarioChoice choiceObject = GameObject.FindObjectOfType<ScenarioChoice>();
        Scenario scenarioToPlay = scenarios[0];
        if (choiceObject)
        {
            scenarioToPlay = scenarios[choiceObject.choice];
        }
        // Give TimelineController the Scenario data
        Queue<ScenarioPoint> scenarioPoints = scenarioToPlay.GetScenarioPoints();
        timelineController.StartTimeline(scenarioPoints);
    }
}
