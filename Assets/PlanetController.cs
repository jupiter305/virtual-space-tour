﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    public MoonOrbit moon;

    public void SetMoonTime(float moonStartTime)
    {
        moon.startTime = moonStartTime;  // It's ok. I'm not really sure how the moon works in the equation either.
    }
}
