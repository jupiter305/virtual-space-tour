﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotation : MonoBehaviour
{
    private float timeScale;  // Multiplier for time scale
    public float baseRotationPerSecond = 0.004f;  // roughly reached from 360 degrees in 24hrs, 60 mins/hour, 60sec/min, 60frame/sec
    private float degreesToRotatePerFrame;
    private GameObject sim;

    // Start is called before the first frame update
    void Start()
    {
        sim = GameObject.Find("SimObjects");  // Get sim controller object
    }

    // Update is called once per frame
    void Update()
    {
        // Update amount to rotate
        timeScale = sim.GetComponent<SimController>().timeScale;
        degreesToRotatePerFrame = baseRotationPerSecond * timeScale;

        // Apply rotation
        transform.Rotate(0, degreesToRotatePerFrame, 0, Space.Self);  
        //Vector3 to = new Vector3(amountToRotatePerFrame, 0, 0);
        //transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, to, Time.deltaTime);
    }
}
