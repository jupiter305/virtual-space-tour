﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public RotatingMenu MainMenu, OptionsMenu, ScenarioMenu;

    private void Start()
    {

    }

    private void DeactivateAllMenus()
    {/*
        MainMenu.SetActive(false);
        OptionsMenu.SetActive(false);
        ScenarioMenu.SetActive(false);*/
    }

    private void GoToMenu(GameObject target)
    {
        // Sends all menus to the back then activates the target menu, sending it to the front
        
    }
    private void RotateAllBack()
    {

        MainMenu.front = false;
        OptionsMenu.front = false;
        ScenarioMenu.front = false;
    }

}
