﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLayer : MonoBehaviour
{
    public int LayerNumberToSet = 0;
    // Sets layer on start.
    void Start()
    {
        gameObject.layer = LayerNumberToSet;
    }
}
