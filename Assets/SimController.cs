﻿// Controls time passed and how quickly time passes.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimController : MonoBehaviour
{
    public float timePassed = 0;  // Time in seconds
    public float timeScale = 8;  // Multiplier of time. Determines speed of simulation.
    public float minTimeScale = 1;
    public float maxTimeScale = 2000;
    public bool showInfo;  // Whether to show time info on screen
    public GameObject timeScaleObject, timePassedObject;
    public Scrollbar scrollbar;
    public bool userCanChangeTimescale = false;
    private int tempTime, seconds, minutes, hours, days;
    Text textComponent, timePassedComponent;
    
    // Start is called before the first frame update
    void Start()
    {
        if (showInfo)
        {
            // Show timescale amount on GUI
            textComponent = timeScaleObject.GetComponent<Text>();
            timePassedComponent = timePassedObject.GetComponent<Text>();
        }
        if (scrollbar)
        {
            // Add slider listener
            scrollbar.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Minimum timescale is 1
        timeScale = Mathf.Max(timeScale, minTimeScale);

        // Tick clock up by timeScale. (divided by frames per second)
        timePassed += timeScale;

        // Calculate time passed in days, hrs, etc.
        tempTime = (int)timePassed;
        days = tempTime / 86400;
        tempTime %= 86400;
        hours = tempTime / 3600;
        tempTime %= 3600;
        minutes = tempTime / 60;
        tempTime %= 60;
        seconds = tempTime;

        if (showInfo)
        {
            // Update text labels
            textComponent.text = "Time Scale: x" + (int)timeScale*60;  // Updating every frame may cause performance issues? 
            timePassedComponent.text = "Time Passed: " + days + " days, " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds";
        }
    }

    // Set timescale to scrollbar value
    public void ScrollBarToTimeScale()
    {
        if (userCanChangeTimescale == true)
        {
            timeScale = scrollbar.value * maxTimeScale;
        }
    }

    public void SetTimescale(float newTimescale)
    {
        // Set timescale and update scrollbar value.
        timeScale = newTimescale;
        scrollbar.value = newTimescale / maxTimeScale;
    }

    // Delegated by slider listener
    private void ValueChangeCheck()
    {
        ScrollBarToTimeScale();
    }
}
