﻿// A hardcode hack method of passing data from this scene to Space
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioChoice : MonoBehaviour
{
    public int choice = 0;  // 0 is solar, 1 is lunar
    public void Choose(int ch)
    {
        choice = ch;
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
