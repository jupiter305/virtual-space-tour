﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenarioScenes : MonoBehaviour
{
    // Hardcoded routes for now. Will upgrade when we have a bigger list of scenarios
    public void GoToTutorial()
    {
        SceneManager.LoadScene("", LoadSceneMode.Single);
        // caution, LoadSceneAsync may be a better alternative
    }
    public void GoToSolar()
    {
        SceneManager.LoadScene("Space", LoadSceneMode.Single);
    }

    public void GoToLunar()
    {
        SceneManager.LoadScene("Space Lunar Eclipse", LoadSceneMode.Single);
    }
}
