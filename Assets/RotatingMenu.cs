﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingMenu : MonoBehaviour
{
    public bool front = false;
    float smooth = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        MoveToPosition();

        if (front == false)
        {
            transform.Rotate(0, 180, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveToPosition();
    }

    void MoveToPosition()
    {
                    // Intended to be used to swing around the user's head when changing menu. Currently instantly changes

        /*if (front == true)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (front == false)
        {
            transform.rotation = Quaternion.Euler(0, 179, 0);
        }*/
    }

    public void SetFront(bool set)
    {
        front = set;
        transform.Rotate(0,180,0);
    }
}
