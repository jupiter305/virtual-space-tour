﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonOrbit : MonoBehaviour
{
    public float startTime = 0;  // Time
    private float timeScale = 8;  // Timescale multiplier
    private float startPosition = 0;  // Angle to offset the start position, in degrees
    private float facingSide = 180;  // Side that always faces earth
    private float spaceScale = (float)0.0001;
    private float E, elapsedTime, parentX, parentY, parentZ;
    private Vector3 newPos;
    private GameObject parentBody;
    private GameObject sim;

    // Start is called before the first frame update
    void Start()
    {
        sim = GameObject.Find("SimObjects");
        parentBody = GameObject.Find("Earth");  // Get Earth object to orbit around
    }

    // Update is called once per frame
    void Update()
    {
        // Update our timescale from the sim controller
        timeScale = sim.GetComponent<SimController>().timeScale;

        // Black magic orbital equations
        float semimajor = 384748 * spaceScale;
        float semiminor = 383800 * spaceScale;
        float eccentricity = Mathf.Sqrt(1 - Mathf.Pow(semiminor,2) / Mathf.Pow(semimajor,2));
        float e = eccentricity;
        float P = 28 * 24;  // Orbit period in hours (28 days)
        float n = 360 / P;  //
        float M = n * (elapsedTime + startTime);
        E = M;
        for (var k = 1; k < n; k++)
        {
            E = M + e * Mathf.Sin(E);
        }
                // MISSING: orbit normal angles (ellipse should not be 2D)
                // MISSING: orbit center offset (ellipse should not be 100% centered on Earth)

        // Apply equation to coords
        elapsedTime += (float)n/60/60/60*timeScale;  // Should use a unified elapsed time instead... TBD
        parentX = parentBody.transform.position[0] + (semimajor * Mathf.Cos(E - e));
        parentY = parentBody.transform.position[1];
        parentZ = parentBody.transform.position[2] + (semiminor * Mathf.Sin(E));
        newPos = new Vector3(parentX, parentY, parentZ);
        transform.position = newPos;


        // Rotate to face Earth (tidal lock)
        transform.LookAt(parentBody.transform);
        transform.rotation *= Quaternion.AngleAxis(270, Vector3.up);  // Offsets by 270 degrees (texture is off by that amount)
    }
}
