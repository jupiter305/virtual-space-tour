﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beeping : MonoBehaviour
{
    public AudioSource audioData;
    public float repeatRate = 1.0f;
    public float delayBeforeRunning = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Init audio
        audioData = GetComponent<AudioSource>();
        // Repeat the sound
        InvokeRepeating("PlayBeep", delayBeforeRunning, repeatRate);
    }

    // Play sound
    void PlayBeep()
    {
        audioData.Play(0);
    }
}
