﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Hacky way to make "hold down button to return to menu"
public class ReturnToMenu : MonoBehaviour
{
    int pressTimer = 0;
    bool pressedNow = false;
    public int pressThreshold = 300;

    public void Pressed() {
        pressedNow = true;
    }
    public void Released() {
        pressedNow = false;
        pressTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (pressedNow == true) {
            pressTimer += 1;
        }
        if (pressTimer >= pressThreshold) {
            SceneManager.LoadScene("Menu", LoadSceneMode.Single);
            pressTimer = 0;
        }
    }
}
