﻿// Unimplemented. Collects audio sources and plays them on command

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    private AudioSource[] audioFiles;

    private void Start()
    {
        audioFiles = GetComponents<AudioSource>();
    }

    void PlayVoiceover(string voiceoverName)
    {
        // Play voiceover here
        Debug.Log("Play sound here. Str: " + voiceoverName);
    }
}
