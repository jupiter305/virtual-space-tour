﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtitleController : MonoBehaviour
{
    public UnityEngine.UI.Text SubtitleTextObject;
    public GameObject panel;
    public float TimeToShow = 5;  // Amount of time the text stays on screen.

    private void Start()
    {
        SubtitleTextObject.text = "";
        panel.SetActive(false);
    }

    // Show text for time equal to TimeToShow.
    public void ShowText(string textToAppear)
    {
        SubtitleTextObject.text = textToAppear;
        panel.SetActive(true);
        StartCoroutine("ResetText", TimeToShow);
    }

    // Clears text after x seconds.
    IEnumerator ResetText(float time)
    {
        yield return new WaitForSeconds(time);
        ClearText();
        yield return null;
    }

    // Stop showing subtitle.
    // Put fadeout here when possible.
    public void ClearText()
    {
        SubtitleTextObject.text = "";
        panel.SetActive(false);
    }
}
